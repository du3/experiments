#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/select.h>

#define big_buf 10

struct tm start;

int http_eol(int fd) {
	int r;
	char s = '\r';
	r = write(fd, &s, 1);
	s = '\n';
	r += write(fd, &s, 1);
	return r;
}

int http_puts(char *str, int fd) {
	int r = -1;
	int l = strlen(str);
	int i;
	for(i = 0; i < l; i++){
		r += write(fd, &str[i], 1);
	}

	r += http_eol(fd);
	return r;
}

void clear_s(char *s) {
	int l = strlen(s);
	int i;
	if(l > 0) {
		for(i = 0; i < l; i++) {
			s[i] = '\0';
		}
	}
}

int log_all_the_stuff(struct tm *adesso) {
	char x[big_buf];
	char *path = "log.txt";
	FILE *logf;
	time_t loc_now;
	logf = fopen(path, "a");
	time(&loc_now);
	/*
	struct tm adesso;
	*/
	/*
	localtime_r(&loc_now, adesso);
	*/
	char str[34];
	int l = 0;
	int i;
	size_t sz = 0;
	int fd_stdin = fileno(stdin);
	int feof_times = 0;
	int n = 0;
	fd_set readf;
	struct timeval ti;

	FD_ZERO(&readf);
	FD_SET(fd_stdin, &readf);
	ti.tv_sec = 1;
	ti.tv_usec = 0;
	if(!logf) {
		return -1;
	}

	fprintf(stderr, "\n\n\n%d %d %d %d %d %d\n\n\n", adesso->tm_sec, adesso->tm_min, adesso->tm_hour, adesso->tm_mday, adesso->tm_mon, adesso->tm_year);
	strftime(&str[0], 34, "%a, %d %b %Y %T %Z", adesso);
	/*
	n = strlen(str);
	n--;
	str[n] = '\0';
	*/
	n = 0;

	putc('[', logf);
	l = strlen(str);
	for(i = 0; i < l; i++) {
		putc(str[i], logf);
	}
	putc(']', logf);
	putc('\n', logf);
	while(feof_times < 1) {
		n = select(1, &readf, NULL, NULL, &ti);
		if(n < 1) feof_times++;
		else {
			/*fread_unlocked(x, 1, big_buf, stdin);*/
			fread(x, 1, big_buf, stdin);
			fwrite(x, 1, strlen(x), logf);
			if(feof_times > 0) feof_times--;
		}
		clear_s(x);
	}
	fputs("end", logf);
	putc('\n', logf);
	fclose(logf);
	return 0;
}

int main (int argc, char *argv[]) {
	start.tm_sec = 1;
	setenv("TZ", "UTC", 1);
	tzset();
	char *content = "<html><head><title>Test Page</title></head><body><h1>It works!</h1><p>Prot test page.</p></body></html>";
	int len = 103;
	char now_s[34];
	char date[36];
	time_t now;
	time(&now);
	struct tm *t;
	t = calloc(1, sizeof(struct tm *));
	/*
	memcpy(t, &start, sizeof(start));
	t = localtime(&now);
	*/
	localtime_r(&now, t);
	char eot = 0x04;
	int n = 0;
	fputs("Init.", stderr);
	fprintf(stderr, "\n\n\n%d\n\n\n", now);
	fprintf(stderr, "\n\n\n%d %d %d %d %d %d\n\n\n", t->tm_sec, t->tm_min, t->tm_hour, t->tm_mday, t->tm_mon, t->tm_year);
	log_all_the_stuff(t);
	/*
	tzset();
	*/
	strftime(now_s, 34, "%a, %d %b %Y %T %Z", t);
	/*ctime_r(&now, &now_s[0]);
	n = strlen(now_s);
	n--;
	now_s[n] = '\0';
	*/

	sprintf(date, "Date: %s", now_s);
	http_puts("HTTP/1.1 200 OK", 1);
	http_puts("Server: prot", 1);
	http_puts(date, 1);
	http_puts("Content-Length: 103", 1);
	http_puts("Content-Type: text/html; charset=utf-8", 1);
	http_eol(1);
	http_puts(content, 1);
	fputs("end.", stderr);
	/*
	write(1, &eot, 1);
	http_puts("", stdout);
	http_puts("", stdout);
	http_puts("", stdout);
	*/

    return 0;
}
