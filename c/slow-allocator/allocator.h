#define List struct linked_list_t

List {
	int length;
	void *back;
	void *next;
	int type;
	/* 
		d = datum
		if type == HEAD, d contains the end of the list
	 */
	void *d; 
};

enum list_types { HEAD, INT, CHAR, STRING };
