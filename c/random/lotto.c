#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#define ln 10
#define end 40

int main (int argc, char *argv[]) {
	char buf[ln];
	int f = open("/dev/urandom", O_RDONLY);
	int i = 0;
	int j = 0;
	int sl = 0;
	int idx = 0;
	int tot = 0;
	int space = 0;
	int ninemode = 0;
	int filter = 0;
	while(tot < end) {
		read(f, buf, ln);
		sl = strlen(buf);
		for(j = 0; ((j < sl) && (tot < end)); j++) {
			if(ninemode) {
				filter = (buf[j] == '0');
			}
			else {
				filter = (buf[j] >= 48) && (buf[j] <= 57);
			}
			if(filter) {
				putchar(buf[j]);
				if(idx == 0) {
					ninemode = buf[j] == '9';
				}

				idx++;
				tot++;
			}
			if(idx > 1) {
				ninemode = 0;
				if(space) {
					space = 0;
					putchar('\n');
				}
				else {
					space++;
					putchar(' ');
				}
				idx = 0;
			}
		}
	}


	//read(f, buf, 10);
	//puts(x);
	close(f);

    return 0;
}
