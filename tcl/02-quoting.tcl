proc q {s} {
	set m [regsub -all {'(.+)'} $s "\x7B\\1\x7D"]
	set m [regsub {''} $m {'}]

	return $m
}

puts [q { asbashbad 'ad''a
	adsdasda
sdsd'}]

eval {puts [q {lgfkgfoog 'asss'}]}
