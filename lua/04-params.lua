function mono()
	print 'void';
end

function one(a)
	print(a..'!')
end

function parens(s)
	return "(".. s ..")"
end

function two(a, b)
	print(a..'first, '..b..'second')
end

--mono
print '1'
one 'abd'
print(table.concat({'mmm', parens 'oooh', 'nnn'}, ' '))
two('1   ', parens 'mmm')
-- woof = 'abcdef'
-- print woof
-- one woof
