BEGIN {
	indentation_level = 0
	is_header = 0
	is_footer = 0

}

function fill(ch, times) {
	r = ""
	for(n = 0; n < times; n++) {
		r = r ch
	}

	return r
}

function realign() {
	l = 2 * indentation_level 
	if(indentation_level > 0)
		gsub(/^[ \t]*/, "")
	
	$0 = fill(" ", l) $0
}

/[{][ \t]*$/ {
	realign()
	indentation_level++
	gsub(/[{][ \t]*$/, "")
	is_header = 1
}
/^[ \t]*[}][ \t]*$/ {
	realign()
	indentation_level--
	if(indentation_level < 0)
		indentation_level = 0
	gsub(/[}][ \t]*$/, "")
	is_header = 0
	is_footer = 1
}

{
	if(!(is_header || is_footer)) {
		realign()
	}
	else {
		is_header = 0
		is_footer = 0
	}

	print
}
