function set(arr, key, value) {
	arr[key] = value
}

BEGIN {
	meta[""] = ""
}

END {
	set(meta, "a", "bcd")
	for(k in meta) {
		print "val: " k " = " meta[k]
	}
}
