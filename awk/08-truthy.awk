function cond(ex, str) {
	if(ex)
		print str
}

END {
	cond(1, 1 "true")
	cond(0, 0 "true")
	cond("", "<> true")
	cond("1", "'1' true")
	cond("0", "'0' true")
}
