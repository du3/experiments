function st(key, value) {
	values[key] = value
	return "x"
}

function pipe(head, str) {
	print str
}

BEGIN {
	values[""] = ""
}

END {
	pipe(st("a", "bcd") st("b", "cdefghij"), "prova prova");
	for(k in values) {
		print "val: " k " = " values[k]
	}
}
