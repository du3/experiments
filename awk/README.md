Chiamate a funzione senza parentesi
===================================
Non funzionano, mannaggia.


Chiamate a funzione multiline
=============================
Funzionano SE la chiusura parentesi sta assieme all'ultima linea
Esempio:
```awk
ml(prova,
prova1,
prova2)
```
Un parser curioso.

Cambio valori inline
====================
È possibilissimo cambiare dentro un argomento di una funzione i valori di altro, potendo fare questo:
```awk
schema_table(schema_config("after", "id"),
    join("\n", new_column("int", "price", column_config("nullable") column_config("default", 0)),
                new_column("datetime", "created_at", column_config("nullable"))))
```

^ inoltre si può ridurre quella roba a una funzione set con un array

(purtroppo la roba del c di poter fare espressioni inline non si può fare)
(no, non c'è manco eval)

Truthyness
==========

le stringhe sono sempre true senza cast, 1 è true e 0 è false come nel c.
