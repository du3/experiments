entries
vediamo con un epub.
=======

wow, quanta merda che non serve più!
Rimane il problema degli ingressi array, quelli vanno presi con .getAll('nome input');

sizes
=====

mupdf non prende le larghezze manco per il cazzo in xhtml.
per fare i pagebreak vuole inoltre un div da solo col page break.
vediamo con un epub.

anchors on separate pages
=========================

Non ho provato con l'epub: mi sembra che mupdf se ne sbatta delle larghezze.
Quindi ho fatto pagine separate, le ho stampate da chrome, le ho unite e ho provato a buttar dentro un pdfmark.
Con queste paginette piccole è molto veloce a fare, e dopo un po' di menate ci sono riuscito pure. mupdf ignora gli spostamenti quindi switch sempre a pagina.
Sono tentato di farli col pdfmark invece di farli con un'emulazione di xdotool però gran menata muoverli, e meno male che non ho bisogno di font fighi
ché non ho voglia di cercare come farli.
